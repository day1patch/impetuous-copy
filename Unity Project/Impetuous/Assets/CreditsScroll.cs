﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreditsScroll : MonoBehaviour {

    AudioSource audio;
    RectTransform trans;
    float timeout = 20f;
    float timer;
    bool fading = false;
    float alphaFadeValue = 1;
    public Texture black; //for fade out


    void Awake()
    {
        var audioGM = GameObject.Find("Music");
        audio = audioGM.GetComponent<AudioSource>();
    }

	// Update is called once per frame
	void Update () {
        if (trans)
            trans.localPosition += Vector3.up;
        else
            trans = GetComponent<RectTransform>();

        timer += Time.deltaTime;

        if (timer > timeout && !fading)
        {
            timer = 0f;
            fading = true;
            StartCoroutine("FadeOut");
        }
	}

    IEnumerator FadeOut()
    {
        while (audio.volume > 0f)
        {
            Debug.Log(audio.volume);
            audio.volume -= Time.deltaTime * 0.05f;
            yield return null;
        }
        Application.LoadLevel("MainMenu");
    }

    //void OnGUI()
    //{
    //    while (audio.volume > 0.25f)
    //    {
    //        alphaFadeValue -= Mathf.Clamp01(Time.deltaTime / 5); // fade out in five seconds
    //        GUI.color = new Color(alphaFadeValue, alphaFadeValue, alphaFadeValue, alphaFadeValue);
    //        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), black);
    //    }
    //}
}
