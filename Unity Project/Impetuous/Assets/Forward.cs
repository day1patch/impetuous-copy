﻿using UnityEngine;
using System.Collections;

public class Forward : MonoBehaviour {

	// Use this for initialization
	void Start () {
        transform.LookAt(Vector3.zero);
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.forward * Time.deltaTime * 40);
	}
}
